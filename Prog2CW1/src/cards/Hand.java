/*
* Auther ajc17dru 100199337
* Last modification date 05/02/2019
* Class hand
* serialVersionUID 300
* Description Contains methods needed for creating Hand objects for use in 
              the full game of Whist
*/

package cards;

import java.util.*;

public class Hand implements Iterable {
    
    /**
     * Serialisation ID 300
     */
    private static final long serialVersionUID = 300;
    
    /**
     * Collection of cards used by each player
     */
    private ArrayList<Card> hand;
    
    /**
     * stores all possible values of currently held cards
     */
    private ArrayList<Integer> values;
    
    /**
     * counts of each suit of card current held
     * also stores current number of aces for calculating hand value
     */
    private int clubs = 0;
    private int diamonds = 0;
    private int hearts = 0;
    private int spades = 0;
    private int aces = 0;
    
    /**
     * Default constructor initialises an empty hand with 0 suit counts
     * and 0 hand value
     */
    public Hand()
    {
        this.hand = new ArrayList<>();
        this.values = new ArrayList<>();
        this.values.add(0);
    }
    
    /**
     * Adds cards from a Collection of type Card to current hand and calculates
     * hand value
     * @param cards array to add cards from
     */
    public Hand(Collection<Card> cards)
    {
        this.hand = new ArrayList<>();
        this.values = new ArrayList<>();
        this.values.add(0);
        spades = clubs = diamonds = hearts = 0;
        for(Card c : cards)
        {
            this.hand.add(c);
            this.addSuits(c);
        }
        calculateValues();
    }
    
    /**
     * Adds cards from array of Cards and calculates hand value
     * @param cards 
     */
    public Hand(Card[] cards)
    {
        this.hand = new ArrayList<>();
        this.values = new ArrayList<>();
        this.values.add(0);
        spades = clubs = diamonds = hearts = 0;
        for(Card c : cards)
        {
            this.hand.add(c);
            this.addSuits(c);
        }
        calculateValues();
    }
    
    /**
     * Adds cards from one hand to another and calculates hand value
     * @param h hand to add from
     */
    public Hand(Hand h)
    {
        this.hand = new ArrayList<>();
        this.values = new ArrayList<>();
        this.values.add(0);
        spades = clubs = diamonds = hearts = 0;
        for(Card c : h.hand)
        {
           this.hand.add(c);
           this.addSuits(c);
        }
        calculateValues();
    }
    
    /**
     * Add card to hand and update suit counts and hand value
     * @param c card to add
     */
    public void addCard(Card c)
    {
        this.addSuits(c);
        this.hand.add(c);
        //treat aces as low when adding to deck to get base value in values[0]
        int value = c.getRank().getValue();
        if(value == Card.Rank.ACE.getValue())
        {
            value = 1;
            this.aces++;
        }
        
        int base = values.get(0);

        values.set(0, base+=value);
        
        calculateValues();
    }
    
    /**
     * add a collection of cards to a hand and update suit counts and hand value
     * @param collection Collection of type Card to add from
     */
    public void addCard(Collection<Card> collection)
    {
        for(Card c : collection)
        {
            this.addSuits(c);
            this.addCard(c);
        }
    }
    
    /**
     * Add an existing hand to the current hand
     * @param h hand to add from
     */
    public void addCard(Hand h)
    {
        for(Card c : h.hand)
        {
            this.addSuits(c);
            this.addCard(c);
        }
    }
    
    /**
     * Adds cards from a Card array to current hand
     * @param cards Card array to add from
     */
    public void addCard(Card[] cards)
    {
        for(Card c : cards)
        {
            this.addSuits(c);
            this.addCard(c);
        }
    }
    
    /**
     * Gets number of clubs currently in hand
     * @return int of number of clubs
     */
    public int getClubs()
    {
        return this.clubs;
    }
    
    /**
     * Gets number of diamonds currently in hand
     * @return int of number of diamonds
     */
    public int getDiamonds()
    {
        return this.diamonds;
    }
    
    /**
     * Gets number of hearts currently in hand
     * @return int of number of hearts
     */
    public int getHearts()
    {
        return this.hearts;
    }
    
    /**
     * Gets number of spades currently in hand
     * @return int of number of spades
     */
    public int getSpades()
    {
        return this.spades;
    }
    
    /**
     * Remove card and decrement suit counts
     * @param c card to remove
     * @return true if found and deleted, false if not 
     */
    public boolean remove(Card c)
    {
        if(this.hand.contains(c))
        {
            if(c.getRank() == Card.Rank.ACE)
            {
                this.aces--;
            }
            this.removeSuits(c);
            this.hand.remove(c);
            calculateValues();
            return true;
        }
        return false;
    }
    
    /**
     * Removes cards contained in another hand from current hand
     * @param h hand containing cards to remove from this hand
     * @return true if all cards removed successfully, false if not
     */
    public boolean remove(Hand h)
    {
        /**
         * assumes all cards removed, if one is not found, set to false
         * and continue
         */
        boolean result = true;
        
        for(Card c : h.hand)
        {
            if(this.hand.contains(c))
            {
                if(c.getRank() == Card.Rank.ACE)
                {
                    this.aces--;
                }
                this.removeSuits(c);
                this.hand.remove(c);
            }
            else
            {
                result = false;
            }
        }
        calculateValues();
        return result;
    }
    
    /**
     * removes card from specific index in hand and returns it and updates suit
     * counter
     * @param index index to remove card from
     * @return card that has been removed from hand
     */
    public Card remove(int index)
    {
        Card c = this.hand.get(index);
        if(c.getRank() == Card.Rank.ACE)
        {
            this.aces--;
        }
        this.removeSuits(c);
        this.hand.remove(index);
        calculateValues();
        return c;
    }
    
    /**
     * updates suit counts when new card is added
     * @param c card to update suit counts with
     */
    private void addSuits(Card c)
    {
        switch(c.getSuit())
        {
            case SPADES : this.spades++; break;
            case CLUBS : this.clubs++; break;
            case DIAMONDS : this.diamonds++; break;
            case HEARTS : this.hearts++; break;
        }
    }
    
    /**
     * removes a suit from a hands suit counter
     * @param c card to remove from suit counter
     */
    private void removeSuits(Card c)
    {
        switch(c.getSuit())
        {
            case SPADES : this.spades--; break;
            case CLUBS : this.clubs--; break;
            case DIAMONDS : this.diamonds--; break;
            case HEARTS : this.hearts--; break;
        }
    }
    
    /**
     * Calculates all possible cards values in a hand
     */
    private void calculateValues()
    {
        //base evalue of all cards + aces treated as low stored in values[0]
        int base = this.values.get(0);
        //then for each ace a new value is made copying the lest, but with 
        //on more ace treated as high instead if low
        for (int i = 1; i < aces; i++) 
        {
            values.add(values.get(i-1) + 10);
        }
    }

    /**
     * returns an iterator that iterates in dealt order
     * @return hand iterator
     */
    @Override
    public Iterator<Card> iterator() 
    {
        return new HandIterator();
    }
    
    /**
     * Iterator for iterating over the cards in a hand
     * @param <Hand> hand to iterate over
     */
    private class HandIterator<Hand> implements Iterator<Card>
    {
        int pos = 0;
        
        /**
         * Checks if the Hand has another card left
         * @return boolean true of cards remaining, false if empty
         */
       @Override
       public boolean hasNext() 
       {
           return pos < hand.size();
       }
       
       /**
        * Returns the next card in the hand
        * @return Card in the next position in the Hand
        */
       @Override
       public Card next() 
       {
           return hand.get(pos++);
       }
    }

    /**
     * Sorts hand into ascending order
     */
    public void sort()
    {
        this.hand.sort(new Card.CompareDescending());
    }

    /**
     * sorts hand into Rank order
     */
    public void sortByRank()
    {
        this.hand.sort(new Card.CompareRank());
    }

    /**
     * returns number of cards of given suit in current hand
     * @param s suit to get total of
     * @return total of given suit
     */
    public int countSuit(Card.Suit s)
    {
        int total = 0;

        for(Card c : this.hand)
        {
            if(c.getSuit() == s)
            {
                total++;
            }
        }
        return total;
    }

    /**
     * Returns number of cards of given rank in current hand
     * @param r rank to get total of
     * @return int total of given rank
     */
    public int countRank(Card.Rank r)
    {
        int total = 0;

        for(Card c : this.hand)
        {
            if(c.getRank() == r)
            {
                total++;
            }
        }
        return total;
    }

    /**
     * indicates if a suit is present in current hand
     * @param s suit to search for
     * @return true if present, false otherwise
     */
    public boolean hasSuit(Card.Suit s)
    {
        /**
         * if at no point a match is found, false is returned, otherwise
         * true returned on match
         */
        for(Card c : this.hand)
        {
            if(c.getSuit() == s)
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Creates a string representation of a hand
     * @return string of all cards in hand
     */
    @Override
    public String toString()
    {
       String s= "";
       for(Card c : this.hand)
       {
           s+= c.toString() + ", ";
       }
       return s;
    }
    
    public int size()
    {
        return this.hand.size();
    }

    /**
    * Main for testing
    * @param args command line args if used
    */
    public static void main(String[] args) 
    {
        //test maming hand and adding cards
       Hand h  = new Hand();
       Card c = new Card(Card.Rank.ACE,Card.Suit.CLUBS);
       h.addCard(c);
       h.addCard(new Card(Card.Rank.TWO,Card.Suit.HEARTS));
       h.addCard(new Card(Card.Rank.FOUR,Card.Suit.SPADES));
       h.addCard(new Card(Card.Rank.KING,Card.Suit.SPADES));
       h.addCard(new Card(Card.Rank.ACE,Card.Suit.DIAMONDS));
       
       //test tostring
        System.out.println(h.toString());
       
       //test getting hand iterator and iterating over hand 
       Iterator i = h.iterator();
       System.out.println(i.hasNext());
       System.out.println(i.next());
       System.out.println(i.hasNext());

       //tets removing and rpint card
       Card c2 = h.remove(0);
       System.out.println(c2);
       
       //test getting spades number
       System.out.println(h.getSpades());
       
       //print current hand values
       System.out.println(h.values);
       
       //test remvoving specific card
       boolean r = h.remove(c);
       System.out.println(r);
       System.out.println(h.toString());
       
       //print new hand values
        System.out.println(h.values);
       
        //sort hand into ascending order
        h.sort();
        System.out.println(h.toString());
        
        //sort hand into rank order
        h.sortByRank();
        System.out.println(h.toString());
        
        //test has clubs suit (should be false as clubs removed beforehand)
        boolean r2 = h.hasSuit(Card.Suit.CLUBS);
        System.out.println(r2);
        
        //test counting ranks
        System.out.println(h.countRank(Card.Rank.TWO));
        
        //test counting suits
        System.out.println(h.countSuit(Card.Suit.SPADES));
        
        
    }
} 