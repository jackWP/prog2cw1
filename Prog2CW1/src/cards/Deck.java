/*
* Auther ajc17dru 100199337
* Last modification date 05/02/2019
* Class Deck
* serialVersionUID 49
* Description Contains methods needed for creating Deck objects for use with 
                other classes
*/
package cards;
import java.io.*;
import java.util.*;

public class Deck implements Iterable, Serializable {
    
    /**
     * serialisation UID
     */
    private static final long serialVersionUID = 49;
    
    /**
     * Array of all possible cards, fixed to max of 52
     */
    public ArrayList<Card> deck;
    
    /**
     * Max size for deck
     */
    private final int maxSize = 52;
    
    /**
     * No of cards currently in deck
     */
    private int currentCards = 0;
    
    /**
     * Initialises a new deck, fills with all possible cards, then shuffles
     */
    public Deck()
    {
        this.deck = new ArrayList<>();//initialise deck
        
        for(Card.Rank r : Card.Rank.values())//for each rank
        {
            for(Card.Suit s : Card.Suit.values())//for each suit of that rank
            {
                this.deck.add(new Card(r,s));//add to deck
            }
        }
        Collections.shuffle(deck);
        
        this.currentCards = 52;
    }
    
    /**
     * Gets current number of cards in deck
     * @return int of number of remaining cards in deck
     */
    public int size()
    {
        return this.currentCards;
    }
    
    /**
     * Re-initialises current deck
     * @return Reinitialised deck
     */
    public final Deck NewDeck()
    {
        return new Deck();
    }
    
    /**
     * Creates a new Iterator for a deck
     * @return Iterator of deck type
     */
    @Override
    public Iterator<Deck> iterator()
    {
        return new DeckIterator();
    }
    
    /**
     * Iterator for iterating over a deck in dealing order (top to bottom)
     * @param <Deck> deck to iterate over
     */
    private class DeckIterator<Deck> implements Iterator<Card>
    {
        /**
         * Current position in current deck
         */
        private int pos = 0;
        
        /**
         * determines if there is another card in the deck
         * @return true or false for if there is a card left or not
         */
        @Override
        public boolean hasNext() 
        {
            return this.pos < currentCards;
        }
        
        /**
         * Gets the next Card in the deck
         * @return Next card in deck
         */
        @Override
        public Card next() 
        {
            return deck.get(pos++);
        } 
        
        /**
         * Remove card at current position
         */
        @Override
        public void remove()
        {
            deck.remove(0);//remove card, post-increment postion
            currentCards--; //decrement card total
        }
    }
    
    private class SpadeIterator<Deck> implements Iterator<Card>
    {
        /**
         * Current position in deck
         */
        private int pos = 0;
        
        /**
         * Checks if there is another Spades card in deck
         * @return true if there is a remaining spades, false if not
         */
        @Override
        public boolean hasNext() 
        {
            for(Card c : deck)
            {   //if there is a spades in deck return true
                if(c.getSuit() == Card.Suit.SPADES) return true;
            }
            return false; //otherwise no spades so return false
        }

        /**
         * Iterates in order of the Spades suit
         * @return next card of the Spades suit
         */
        @Override
        public Card next() 
        {
            Card c  = deck.get(pos);
            //while there are cards, and current card is not spades
            while(this.hasNext() && c.getSuit() != Card.Suit.SPADES)
            {
                pos++; //increment position
                c  = deck.get(pos); //get next card
            }
            //exits once Spades card is found and returned, incrementing to
            //next card
            pos++;
            return c;
        }
        
        /**
         * remove card at current position
         */
        @Override
        public void remove()
        {
            deck.remove(0);//remove card
            currentCards--; //decrement card total
        }
    }
    
    /**
     * Deals the next card in the deck, removing it from the deck
     * @return Card to be dealt and removed
     */
    public Card deal()
    {
        DeckIterator<Deck> i = new DeckIterator<>();
        //check if there are cards remaining
        if (i.hasNext())
        {
            Card c = i.next(); //get card
            i.remove(); //remove card from deck
            return c; //return it
        }
        else
        { //otherwise no cards left, print error and continue
            throw new IllegalArgumentException("No cards in deck to deal.");
        }
    }
    
    /**
     * Writes cards in a deck in SpadeIterator order
     * @param out stream to write to 
     * @throws IOException if error occurs with output stream when saving
     */
    public void writeObject(ObjectOutputStream out) throws IOException
    {
        try
        {
            Iterator<Deck> i = new SpadeIterator();//spade iterator order

            while(i.hasNext())
            {
                out.writeObject(i.next()); //write spade cards
            }
        }
        catch(IOException e)//stop and print IO error
        {
            System.out.println("Error saving Deck: " + e);
        }
    }
    
    /**
     * Creates string representation of a deck
     * @return string with information about the deck and its cards
     */
    @Override
    public String toString()
    {
        String s = "Size: " + this.size() + "\nCards: \n";
        
        for (Card c : this.deck)
        {
            s += c.toString() +"\n";
        }
        
        return s;
    }
    
    /**
     * Main for testing
     * @param args command line args if used
     */
    public static void main(String[] args) 
    {
        //initialise a new deck (shuffled)
        Deck deck = new Deck();
        //print full deck and info
        //System.out.println(deck.toString());
        
        //deal card (normal order)
        Card dealTest = deck.deck.get(0); //get copy of first card to check if 
                                        //correct card is dealt
        //test deal
        Card deal = deck.deal();
        System.out.println("Card should be: " + dealTest.toString());
        System.out.println("Dealt card was: "+ deal.toString());
        
        //test new deck size
        System.out.println("Deck size is now: " + deck.size()+"/52");
        
        //test getting a normal iterator
        Iterator<Deck> i = deck.iterator();
        System.out.println("Does the iterator have next: "+i.hasNext());
        
        //Test getting spades iterator
        Iterator<Deck> iSpades = deck.new SpadeIterator();
        System.out.println("Does the Spades iterator have next: "
                + iSpades.hasNext());
        
        //test getting new deck
        deck = deck.NewDeck();
        System.out.println("New first card is: " + deck.deck.get(0).toString());
        System.out.println("Deck size is: " +deck.size());
        
        //tets tostring
        System.out.println(deck.toString());
        
        
        
    }
}