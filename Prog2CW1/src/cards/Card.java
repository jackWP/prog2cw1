/*
* Auther ajc17dru 100199337
* Last modification date 05/02/2019
* Class Card
* serialVersionUID 100
* Description Contains methods needed for creating Card objects for use with 
                other classes
*/

package cards;
import java.util.*;
import java.io.*;


public class Card implements Serializable, Comparable<Card> {
    
    /**
     * serialisation UID
     */
    private static final long serialVersionUID = 100; 
   
    /**
     * Enum of card rank
     * Methods
     * getValue - returns int value of Rank
     * getNext - return Rank of next Rank in list (circular)
     */
    private final Rank rank;
    
     /**
     * Enum of card suit
     * Methods
     * getRandomSuit - returns random suit enum
     */
    private final Suit suit;
    
    /**
     * Comparator class for sorting in DESCENDING order by rank then suit
     */
    public static class CompareDescending implements Comparator<Card>
    {
        /**
         * Compares first card to second by rank
         * @param c1 first card
         * @param c2 second card
         * @return int result of comparison
         */
        @Override
        public int compare(Card c1, Card c2) 
        {
             int res = c2.compareTo(c1);
             
             if (res == 0) //if ranks equal
             {//compare suits and return
                 return c2.suit.ordinal() - c1.suit.ordinal();
             }
             else if (res > 0) 
             {
                 return 1;
             }
             else return -1;
        }
    }
    
    /**
     * Comparator class for sorting in ASCENDING order by rank
     */
    public static class CompareRank implements Comparator<Card>
    {
        /**
         * Compares two cards by rank
         * @param c1 first card
         * @param c2 second card
         * @return int result of comparison
         */
        @Override
        public int compare(Card c1, Card c2) 
        {
            return c1.compareTo(c2);
        }
    }
    
    /**
     * Enum to represent each Cards Rank
     */
    public enum Rank
    {
        TWO(2),
        THREE(3),
        FOUR(4),
        FIVE(5),
        SIX(6),
        SEVEN(7),
        EIGHT(8),
        NINE(9),
        TEN(10),
        JACK(10),
        QUEEN(10),
        KING(10),
        ACE(11);
        
        private final int value; //value for each card
        //list of possible values
        private static final Rank[] vals = Rank.values(); 
        
        /**
         * constructor for constant values
         * @param x int value to assign to constant
         */
        Rank(int x)
        {           
            this.value = x;
        }
        
        /**
         * constructor for enum constant
         * @param r rank to assign constant
         */
        Rank(Rank r)
        {
            value=r.getValue();
        }
        
        /**
         * returns int value associated with card
         * @return int value of card's rank
         */
        public int getValue()
        {
            return this.value;
        }
        
        /**
         * returns the logical next rank of current card (circular)
         * @param rank rank to get next rank of
         * @return the next rank 
         */
        public Rank getNext(Rank rank)
        {
            return vals[this.ordinal()+1 % vals.length];
            //use % on vals length with desired next val to simulate list being 
            //indx out of bounds circular and avoid
        } 
    };

    /**
     *Enum to represent each cards Suit
     */
    public enum Suit
    {
        CLUBS,
        DIAMONDS,
        HEARTS,
        SPADES;
        
        //Array of possible values
        private static final Suit[] vals = Suit.values();
        
        /**
         * returns a randomly selected suit
         * @return random suit 
         */
        public static Suit RandomSuit()
        {
            Random r = new Random();
            return vals[r.nextInt(4)];
        }
        
    };
    
    /**
     * Normal constructor
     * @param r rank of card to create
     * @param s  suit of card to create
     */
    public Card(Rank r, Suit s)
    {
        this.rank = r;
        this.suit = s;
    }
  
    /**
     * gets the rank of the input card
     * @return Rank of Card
     */
    public Rank getRank()
    {
        return this.rank;
    }
    
    /**
     * Returns serial version UID
     * @return ID of type long
     */
    public long getserialVersionUID()
    {
        return serialVersionUID;
    }
    
    /**
     * gets the suit of the input card
     * @return Suit of card
     */
    public Suit getSuit()
    {
        return this.suit;
    }
    
    /**
     * Creates a strong representation of a card
     * @return String representation of card 
     */
    @Override
    public String toString() 
    {
        return  rank + " of " + suit;
    }

    /**
     * compares two cards based on thier ranks value
     * @param c Card to compare to
     * @return Card1 rank - Card2 rank
     */
    @Override
    public int compareTo(Card c) 
    {
        return c.rank.getValue() - this.rank.getValue();
    }
    
    /**
     * gets the highest value card from an input list
     * @param cards List of cards to find max Card rank of
     * @return Card with highest rank
     */
    public static Card max(List<Card> cards)
    {
        Card max = cards.get(0);
        
        Iterator<Card>it = cards.iterator();
        
        while(it.hasNext())
        {
            Card c = it.next();
            
            if(max.compareTo(c) < 0)
            {
                max = c;
            }
        }
        return max;
    }
    
    /**
     * compares a list against a specified card and selects those from the list
     * that are greater in value than the specified card
     * @param cards List of cards to sort
     * @param comp Comparator to use
     * @param c Card to compare to and select greater cards
     * @return List of selected cards
     */
    public static List<Card> chooseGreater
        (List<Card> cards, Comparator comp, Card c)
    {
        List<Card> results = new ArrayList<>();
        
        for(Card card : cards) //for every card in list
        {
            if(comp.compare(c, card) >0) //compare it to given card
            {
                results.add(card); //if card in list is greater, add to results
            }
        }
        return results;
    }
     
    /**
     * Same as chooseGreater, instead using Lambdas, prints out results
     * @param cards List of cards to sort
     * @param comp Comparator to use
     * @param c Card to compare to and select greater cards
     */
    public static void selectTest
        (List<Card> cards, Comparator comp, Card c)
    {
        Comparator<Card> compRank = (Card a, Card b) ->
        {
            return a.getRank().getValue() - b.getRank().getValue();
        };
        
        Comparator<Card> compDesc = (Card a, Card b) ->
        {
            return b.compareTo(a);
        };
        
        Card c1 = new Card(Rank.ACE,Suit.CLUBS);
        
        List<Card> res = chooseGreater(cards,compDesc,c1);
        for(Card card : res)
        {
            System.out.print(c.toString() + ",");
        }
        System.out.println("");
        res.clear();
        res = chooseGreater(cards,compRank,c1);
        for(Card card : res)
        {
            System.out.print(card.toString()+ ",");
        }
    }
    
    /**
     * Test harness
     * @param args command line args if used
     */
    public static void main(String[] args) 
    {
        //test rank and suit
        Suit s = Suit.HEARTS;
        Rank r = Rank.EIGHT;
        //test card initialisation
        Card c1 = new Card (r,s);
        //toString test
        System.out.println("Test tostring: " +c1.toString());
        //test getting values
        System.out.println("Test getting rank value: " +c1.rank.getValue());
        //test getting random suit
        System.out.println("Test getting random suit: " +
                Suit.RandomSuit());
        //Test getting next rank
        System.out.println("Test getting next rank: " 
                +c1.rank.getNext(c1.getRank()));
        //Test card ArrayList
        ArrayList<Card> cards = new ArrayList<>();
        Card c2 = new Card(Rank.TWO,Suit.SPADES);
        Card c3 = new Card(Rank.JACK,Suit.HEARTS);
        Card c4 = new Card(Rank.JACK,Suit.SPADES);
        //test adding cards
        cards.add(c1);
        cards.add(c2);
        cards.add(c3);
        cards.add(c4);
        
        //Test looped print
        for (Card c : cards)
        {
            System.out.println("Test loop print: "+c.toString());
        }
        //test getting rank
        System.out.println("Test getting rank: "+c2.getRank());
        //test getting suit
        System.out.println("Test getting suit: "+c3.getSuit());
        //test getting rank value
        System.out.println("Test getting rank value: "+c3.getRank().getValue());
        
        //test comparisons with comparitors and lambdas
        System.out.println(c1.compareTo(c2));
        
        Comparator<Card> compRank = new CompareRank();
        Comparator<Card> compDescending = new CompareDescending();
        
        System.out.println("Compare Rank: "+ chooseGreater(cards,compRank,c1));
        System.out.println("Compare Descending: "
                +chooseGreater(cards,compDescending,c1));
        selectTest(cards,compRank,c1);
        selectTest(cards,compDescending,c1);
    }  
}