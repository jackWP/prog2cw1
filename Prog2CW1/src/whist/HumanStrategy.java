/*
* Auther : ajc17dru 100199337
* Last modification date : 05/02/2019
* Class : Human Strategy
* Description : Allows a human to play a game of whist using the console
*/

package whist;

import cards.Card;
import cards.Hand;


public class HumanStrategy 
{
    public HumanStrategy()
    {}
    
    /**
     * Allows a person to play cards using the console
     * @param h hand to play from
     * @param t the current trick
     * @return card they wish to play (already removed from the hand)
     */
    public Card chooseCard(Hand h, Trick t) 
    {
        Card c;

        /**
         * print out all relevant information for the hand and the trick
         */
        System.out.println("Current hand: ");
        System.out.println(h.toString());
        System.out.println("You have " + h.getClubs() + " Clubs, " 
                + h.getDiamonds() + " Diamonds, "+h.getHearts() + " Hearts, "
        +h.getSpades() + " Spades.");

        if(t.playedCards[0] == null)
        {
            System.out.println("You are the leading player.");
        }
        else
        {
            System.out.println("The current Trick is:" + t.toString());
            System.out.println("The lead Suit is: " 
                    + t.getLeadSuit().toString());
        }

        System.out.println("The current Trump is: " + 
                t.getTrump().toString());
        System.out.println("To choose a card, type the index of the card you "
                + "whish to play.");
            
        
        //get card that is now in index of the hand
        int index = Integer.parseInt(System.console().readLine());
        c = h.remove(index);

        /**
         * check to see if hand has any of the lead suit, and if the desired
         * card is of the played suit
         */
        int leadCount = h.countSuit(t.getLeadSuit());
        boolean valid = false;
        while(valid == false)
        {
            //check index is in range on subsequent runs of while loop
            if(index < h.size() && index > 0)
            {
                /**
                 * check of there are remaining lead suit cards and if card 
                 * played is of lead suit
                 */
                if(c.getSuit() == t.getLeadSuit() && leadCount > 0)
                {
                    valid = true;
                }
                /**
                 * check if there are no lead cards and card played is not 
                 * of lead suit
                 */
                else if(c.getSuit() != t.getLeadSuit() && leadCount == 0)
                {
                    valid = true;
                }
                else
                {
                    //otherwise read index in again
                    System.out.println("You still have cards of the lead suit: " +
                    t.getLeadSuit().toString());
                    h.addCard(c);
                    c = h.remove(Integer.parseInt(System.console().readLine()));
                }
            }
            else
            {
                //otherwise out of range and read index in again
                System.out.println("Please enter an index between 0 and " +
                        h.size());
                h.addCard(c);
                c = h.remove(Integer.parseInt(System.console().readLine()));
            }
        }
        return c;
    }
}
