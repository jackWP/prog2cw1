/*
* Auther : ajc17dru 100199337
* Last modification date : 05/02/2019
* Class : BasicGame
* Description : Plays a basic game of Whist using supporting Cards package 
*/

package whist;

import cards.Card.*;
import cards.Deck;
import cards.Hand;

public class BasicGame 
{
    static final int NOS_PLAYERS=4;
    static final int NOS_TRICKS=13;
    static final int WINNING_POINTS=7;
    
    int team1Points=0;
    int team2Points=0;
    
    BasicPlayer[] players;
    
    public BasicGame(Player[] pl){
        
    }
    
    public void dealHands(Deck newDeck)
    {
        for(int i=0;i<NOS_TRICKS;i++){
            players[i%NOS_PLAYERS].dealCard(newDeck.deal());
        }
    }
    
    public Trick playTrick(BasicPlayer firstPlayer)
    {
        Trick t=new Trick(firstPlayer.getID());
        int playerID=firstPlayer.getID();
        for(int i=0;i<NOS_PLAYERS;i++)
        {
            int next=(playerID+i)%NOS_PLAYERS;
            t.setCard(players[next].playCard(t),players[next]);
        }
        return t;
    }
    
    public void playGame()
    {
        Deck d=new Deck();
        dealHands(d);
        int firstPlayer=(int)(NOS_PLAYERS*Math.random());
            
        Suit trumps = Suit.RandomSuit();
        
        Trick.setTrumps(trumps);
        for(int i=0;i<NOS_PLAYERS;i++)
            players[i].setTrumps(trumps);
        
        for(int i=0;i<NOS_TRICKS;i++)
        {
            Trick t=playTrick(players[firstPlayer]);            
            System.out.println("Trick ="+t);
            firstPlayer=t.findWinner();
            System.out.println("Winner ="+firstPlayer);
            
            
        }
    }
    
    /**
     * Method to play series of Whist games until the needed number of team 
     * points is reached.
     */
    public void playMatch()
    {
        team1Points=0;
        team2Points=0;
        while(team1Points<WINNING_POINTS && team2Points<WINNING_POINTS)
        {
            playGame();
        }
        if(team1Points>=WINNING_POINTS)
        {
            System.out.println("Winning team is team1 1 with"+team1Points);
        }
        else
        {
            System.out.println("Winning team is team2 1 with"+team2Points);
        }
            
    }
    
    public static void playTestGame()
    {
        BasicPlayer[] p = new BasicPlayer[NOS_PLAYERS];
        for(int i=0;i<NOS_PLAYERS;i++)
        {
            BasicStrategy s = new BasicStrategy();           
            Hand h = new Hand(); 
            System.out.println(h);
            p[i] = new BasicPlayer(h,i,s);
        }
        for(Player pl : p)
        {
            System.out.println(pl);
        }
        BasicGame bg=new BasicGame(p);
        bg.playMatch(); //Just plays a single match
    }
    
    public static void main(String[] args) 
    {
        playTestGame();
    }
}