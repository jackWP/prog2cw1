/*
* Auther : ajc17dru 100199337
* Last modification date :05/02/2019
* Class : Trick
* Description : Class for storing information about whist tricks
*/

package whist;
import cards.Card;
import cards.Card.Suit;
import cards.Deck;
import java.util.*;

public class Trick
{
    /**
    * Stores the current trump Suit
    */
   public static Suit trumps;
   /**
    * Stores the lead suit of the trick
    */
   private Suit leadSuit;
    /**
    * Stores the current players
    */
   private final HashMap<Integer, BasicPlayer> players = new HashMap<>();
   /**
    * Stores the played cards in the Trick
    */
   public Card[] playedCards;
   
   /**
    * No default constructor as no trick should ever be initialised without 
    * having data passed to it
    */
   
   /**
    * Add players from Collection typed to Players to Trick
    * @param players player collection to add from
    * @param trump set trump card
    */
   public Trick(Collection<BasicPlayer> players, Suit trump)
   {    
       for(BasicPlayer p : players)
       {
           this.players.put(p.getID(), p);
       }

       setTrumps(trump);
       
       playedCards = new Card[players.size()];
   }
   
   /**
    * Add players to trick from array
    * @param players players to add to trick
     * @param trump set trump card
    */
   public Trick(BasicPlayer[] players, Suit trump)
   {
       for (int i = 0; i < players.length; i++) 
       {
           this.players.put(players[i].getID(), players[i]);
       }

       setTrumps(trump);
       
       playedCards = new Card[players.length];
   }
   
   /**
    * This was in the code provided but I can't find a purpose for it 
    * @param p 
    */
   public Trick(int p)
   {
   }    
   
   /**
    * Set the current trump card for the trick
    * @param s Suit to set trump as
    */
   public static void setTrumps(Suit s)
   {
       trumps=s;
   }
    
/**
 * Deals a card and returns suit for the lead card
 * @return the Suit of the lead card.
 * @throws NullPointerException When no cards have been played, exception occurs
 */    
    public Suit getLeadSuit() throws NullPointerException
    {
        /**
         * If no cards have been played a null pointer exception will occur
         * when trying to access playedCards
         */
        return this.playedCards[0].getSuit();
    }
/**
 * Records the Card c played by Player p for this trick
 * @param c Card to record
 * @param p player who played the card
 */
    public void setCard(Card c, BasicPlayer p)
    {
        this.playedCards[p.getID()-1] = c;
    }
/**
 * Returns the card played by player with id p for this trick
 * @param p Player to get card played by
 * @return Card played by given player
 */    
    public Card getCard(BasicPlayer p)
    {
        return this.playedCards[p.getID()-1];  
    }
    
/**
 * Finds the ID of the winner of a completed trick
 * @return id of winning player
 */    
    public int findWinner() 
    {
        //start winning id as lead card
        int winningID = 0;
        
        for (int i = 0; i < this.playedCards.length; i++) 
        {
            Card current = this.playedCards[i+1];
            
            //check for trumps first
            if(current.getSuit() == trumps)
            {
                if(current.compareTo(this.playedCards[winningID]) > 0)
                {
                    winningID = i;
                }
            }
            //if not trumps, check greater lead suit
            //anything not lead suit is ignored
            //if nothing beats lead suit, lead card wins automatically
            else if(current.getSuit() ==  getLeadSuit())
            {
                if(current.compareTo(this.playedCards[winningID]) >0 )
                {
                    winningID = i;
                }
            }
        }  
        return winningID;
    }
    
    /**
     * Creates a string representation of a Trick
     * @return String representation of the trick
     */
   @Override
    public String toString()
    {
        String s = "";
        
        for(Card c : this.playedCards)
        {
            s+= c.toString() + ',';
        }
        return s;
    }
    
    /**
     * Gets the current trump of the trick
     * @return Suit of the current trump
     */
    public Suit getTrump()
    {
        return trumps;
    }
    
    /**
     * TESTING FOR TRICK AND PLAYER
     * @param args 
     */
    public static void main(String[] args)
    {
        Deck d = new Deck();
        
        //test making players
        BasicPlayer p1 = new BasicPlayer();
        BasicPlayer p2 = new BasicPlayer();
        BasicPlayer p3 = new BasicPlayer();
        BasicPlayer p4 = new BasicPlayer();
        
        ArrayList<BasicPlayer> pList = new ArrayList<>();
        
        pList.add(p1);
        pList.add(p2);
        pList.add(p3);
        pList.add(p4);
        
        //deal cards to players
        for(BasicPlayer p : pList)
        {
            for(int i = 0; i < 13; i++)
            {
                p.dealCard(d.deal());
            }
        }
        
        //test making Trick
        Trick t = new Trick(pList,Card.Suit.RandomSuit());
        
        //test playing card
        Card c  = p1.playCard(t);
        
        //test getting id
        int p1ID = p1.getID();        
        
        
        //TEST PLAYING CARDS FOR EACH PLAYER (t.setcards)
        //TEST GETTING PLAYED CARD BY PLAYER ID
        //TEST FINDING WINNER
        //TEST PRITING PLAYERS
        
        
        
        //Test getting the leading suit
        System.out.println(t.getLeadSuit());
        
        
        
    }
}
