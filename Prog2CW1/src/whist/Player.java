/*
* Auther : ajc17dru 100199337
* Last modification date : 05/02/2019
* Class : Player
* Description : Interface for a Whist Player 
*/
package whist;
import cards.Card;
import cards.Card.Suit;

public interface Player {
/**
 * Adds card c to this players hand
 * @param c card to add to players hand
 */    
    void dealCard(Card c);
    
/** 
 * Allows for external setting of player strategy
 * @param s strategy to set to player
 */    
    void setStrategy(Strategy s);
/**
 * Determines which of the players cards to play based on the trick t 
 * and player strategy
 * @param t Trick ot play card to
 * @return card to play
 */
    Card playCard(Trick t);
/**
 * Game passes the players the completed trick 
 * @param t trick to pass to player
 */    
    void viewTrick(Trick t);
    
    /**
     * ????????????????????
     * @param s 
     */
    void setTrumps(Suit s);
    
    /**
     * gets the ID of the player
     * @return int ID of the player
     */
    int getID();
}
