/*
* Auther : ajc17dru 100199337
* Last modification date : 05/02/2019
* Class : Player
* Description : Class for a Whist BasicPlayer implmementing Player interface
*/

package whist;

import cards.Card;
import cards.Hand;
import java.util.*;

public class BasicPlayer implements Player
{
    /**
     * Hand for each players cards
     */
    private Hand hand;
    /**
     * ID to identify each player
     */
    private final int playerID;
    /**
     * This controls how each player will play cards
     */
    private Strategy strategy;
    /**
     * Allows viewing of the completed trick
     */
    private Trick completeTrick;
    
    /**
     * Initialise an empty Player
     */
    public BasicPlayer()
    {
        this.hand = new Hand();
        this.playerID = 0;
        this.strategy = new BasicStrategy();
    }
    
    /**
     * Initialise a player with a hand, ID and strategy
     * @param h Hand to give Player
     * @param ID int ID to give player
     * @param s Strategy for Player to use
     */
    public BasicPlayer(Hand h, int ID, BasicStrategy s)
    {
        if(h.size() != 0)
        {
            this.hand.addCard(h);
        }

        this.playerID = ID;
        this.strategy = s;
    }
    
    /**
     * Initialise a Player with a Collection<Card> as hand, ID and strategy
     * @param hand Collection<Card> to use as Hand
     * @param ID int ID to give player
     * @param s Strategy for player to use
     */
    public BasicPlayer(Collection<Card> hand, int ID, BasicStrategy s)
    {
        this.hand.addCard(hand);
        this.playerID = ID;
        this.strategy = s;
    }
    
    /**
     * Initialises a PLayer with a Card Array as hand, ID and Strategy
     * @param hand Card array to use as hand
     * @param ID int id to give PLayer
     * @param s Strategy for player to use
     */
    public BasicPlayer(Card[] hand, int ID, BasicStrategy s)
    {
        this.hand.addCard(hand);
        this.playerID = ID;
        this.strategy = s;
    }
    
    /**
     * Adds a card to the players hand
     * @param c Card to add to the hand
     */
    @Override
    public void dealCard(Card c) 
    {
        this.hand.addCard(c);
    }

    /**
     * Sets the current player strategy
     * @param s strategy to set to player
     */
    @Override
    public void setStrategy(Strategy s) 
    {
        this.strategy = s;    
    }

    /**
     * Plays a card based on the current strategy
     * @param t Trick to play card to
     * @return Card that has been played and removed from the players hand
     */
    @Override
    public Card playCard(Trick t) 
    {
        return this.strategy.chooseCard(hand, t);
    }

    /**
     * Passes the player the completed Trick to view it
     * @param t Trick to give to player
     */
    @Override
    public void viewTrick(Trick t) 
    {
        this.completeTrick = t;
    }

    /**
     * I don't know what this function is needed for, so I have left it in
     * in case it is on the spec or something
     * @param s 
     */
    @Override
    public void setTrumps(Card.Suit s) 
    {}
    
    /**
     * returns the players ID
     * @return int player ID
     */
    @Override
    public int getID() 
    {
       return this.playerID;
    }
    
    /**
     * Prints out player ID and its hand
     * @return string containing player ID and its hand
     */
    @Override
    public String toString()
    {
        return "Player: "+ this.getID() + this.hand.toString();
    }
    
    /**
     * TESTING FOR PLAYER IS IN TRICK AS ONE REQUIRES THE OTHER TO BE TESTED
     * @param args 
     */
    public static void main(String[] args)
    {}
}
