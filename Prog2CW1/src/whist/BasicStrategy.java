/*
* Auther : ajc17dru 100199337
* Last modification date : 05/02/2019
* Class : Player
* Description : Class for a Whisy BasicStrategy implementing Strategy
*/

package whist;

import cards.Card;
import cards.Hand;
import java.util.*;


public class BasicStrategy implements Strategy
{

    /**
     * Current trick to base decisions on
     */
    private Trick currentTrick;
    private Hand hand;
    private Card.Suit leadSuit;
    private Card.Suit trump;
    
    /**
     * Makes a new strategy 
     */
    public BasicStrategy()
    {
    }
    
    /**
     * Decides which card to play based on the current hand and Trick
     * @param h hand to choose cards from
     * @param t trick to make decisions based on with the current hand
     * @return chosen card to play
     */
    @Override
    public Card chooseCard(Hand h, Trick t) 
    {
        updateData(t);
        Card chosen = null;
        Card.Suit lead  = t.getLeadSuit();
        Card.Suit trumps = t.getTrump();
        
        boolean canFollowLead = this.hand.countSuit(trump) > 0;
        boolean canTrump = this.hand.countSuit(trump) > 0;
        int turn = 0;
        
        //number of cards in played cards determines the turn currently on
        for(Card c : t.playedCards)
        {
            turn ++;
        }
        
        switch(turn)
        {
            /**
             * if first player, play highest ranking card
             */
            case 0 : 
                h.sortByRank();
                chosen =  h.remove(0);
            /**
             * if second player
             */
            case 1:
                //if player can follow lead suit
                if(canFollowLead)
                {
                    h.sortByRank();
                    Card c;
                    
                    Iterator i = h.iterator();
                    
                    while(i.hasNext())
                    {
                        c = (Card) i.next();
                        //find highest ranking lead suit card
                        if(c.getSuit() == lead)
                        {
                            //check of card is more than lead card
                            if(c.getRank().getValue() > 
                                    t.playedCards[0].getRank().getValue())
                            {
                                chosen = c;
                                break;
                            }
                            //otherwise set lowest value card to chosen
                            else chosen = c;
                        }
                    }
                }
                //if player cant follow lead but can trump
                else if (canTrump)
                {
                    Iterator i = h.iterator();
                    h.sortByRank();
                    
                    //set chosen card to highest trump
                    while(i.hasNext())
                    {
                        Card c = (Card) i.next();
                        
                        if(c.getSuit() == trumps)
                        {
                            chosen = c;
                            break;
                        }
                    }
                }
                //if player cannot follow lead or trump, discard lowest card
                else
                {
                    h.sortByRank();
                    chosen = h.remove(h.size()-1);
                }
                break;
            /**
             * if third player (partner has played), 
             */
            case 2:
                //if can follow lead, play lowest card
                if(canFollowLead)
                {
                    Iterator i = h.iterator();
                    Card c;
                    while(i.hasNext())
                    {
                        c = (Card) i.next();
                        if(c.getSuit() == lead)
                        {
                            chosen = c;
                        }
                    }
                }
                //otherwise discard lowest non-trump
                else
                {
                    h.sortByRank();
                    
                    Iterator i = h.iterator();
                    
                    while(i.hasNext())
                    {
                        Card c = (Card) i.next();
                        
                        if(c.getSuit() != trumps)
                        {
                            chosen = c;
                        }
                    }
                }
                
                break;
            /**
             * if final (fourth) player, partner played first. Make sure not to
             * trump ally 
             */
            case 3:
                //if can follow lead, play lowest card of lead suit
                if(canFollowLead)
                {
                    h.sortByRank();
                    
                    Iterator i = h.iterator();
                    
                    while(i.hasNext())
                    {
                        Card c = (Card) i.next();
                        
                        if(c.getSuit() == lead)
                        {
                            chosen = c;
                        }
                    }
                }
                //otherwise play lowest card 
                else
                {
                    h.sortByRank();
                    Iterator i = h.iterator();
                    
                    while(i.hasNext())
                    {
                        chosen = (Card) i.next();
                    }
                }
                break;
        }
        
        h.remove(chosen);
        return chosen;
    }

    /**
     * Updates the held trick so chooseCard can make decisions
     * @param t trick to update strategy with
     */
    @Override
    public void updateData(Trick t) 
    {
        this.currentTrick = t;
        this.leadSuit = t.getLeadSuit();
        this.trump = t.getTrump();
    }
    
    /**
     * TETSING IN TRICK AS ONE REQUIRES THE OTHER TO TEST
     * @param args 
     */
    public static void main(String[] args)
    {}
}
