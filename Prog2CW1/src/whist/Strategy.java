/*
* Auther : ajc17dru 100199337
* Last modification date : 05/02/2019
* Class : Strategy
* Description : Interface for AI stratagy when playing Whist
*/

package whist;
import cards.Card;
import cards.Hand;


public interface Strategy 
{
/**
 * Choose a card from hand h to play in trick t 
 * @param h
 * @param t
 * @return 
 */    
    Card chooseCard(Hand h, Trick t);
/**
 * Update internal memory to include completed trick c
 * @param c 
 */    
    void updateData(Trick c);
}
